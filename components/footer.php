<footer class="bg-color2 mt-10">
  <div class="max-w-6xl p-4 py-6 mx-auto lg:py-16 md:p-8 lg:p-10">
    <div class="grid grid-cols-1 gap-8 md:grid-cols-3 lg:grid-cols-4">
      <div>
        <div class="justify-center flex">
          <img src="<?php echo $URI->base('/assets/img/logo_white.png') ?>">
        </div>
      </div>
      <div>
        <h3 class="mb-6 text-sm font-semibold text-white uppercase">
          WWW.VLCONSULT.COM.BR
        </h3>
        <ul class="text-white">
          <li class="mb-4">
            <a href="#" class="hover:underline">Serviços</a>
          </li>
          <li class="mb-4">
            <a href="#" class="hover:underline">Quem Somos</a>
          </li>
        </ul>
      </div>
      <div>
        <h3 class="mb-6 text-sm font-semibold text-white uppercase">Contato</h3>
        <p class="text-white">
          Telefone: 800 888 4004
          Endereço: Av. Principal do Mocambinho, Mocambinho, Teresina - PI, 64000-000, Brasil
          E-mail: somosvl@gmail.com
        </p>
      </div>
      <div>
        <a href="https://wa.me/558008884004">
          <i class="bi bi-whatsapp"></i>
        </a>
        <a href="https://www.instagram.com/somos.vl">
          <i class="bi bi-instagram"></i>
        </a>
        <a href="https://web.facebook.com/somos.vlconsult">
          <i class="bi bi-facebook"></i>
        </a>
      </div>
    </div>
    <div class="text-center pt-10">
      <span class="block text-base text-center text-white">© 2023 - VL Consultoria e Negócios
        <br>
        CNPJ 12.840.979/0001-95
      </span>
      <div class="pt-10">
        <span class="block text-xs text-center text-white">Site criado por
        </span>
        <span class="block text-xs text-center text-white">Web Developer Full Stack: @cairofelipedev
        </span>
      </div>
    </div>
  </div>
</footer>

<script src="<?php echo $URI->base('/assets/js/dark_mode.js') ?>"></script>
<script src="https://unpkg.com/flowbite@1.4.1/dist/flowbite.js"></script>